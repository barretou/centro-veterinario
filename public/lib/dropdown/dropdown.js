;(function() {
  let dropdown = {
    current: null,

    isOpen: false,

    init: (props) => {
      const trigger = (props.element === undefined) ? '.dropdown-button' : props.element

      document.addEventListener('click', (e) => {
        e.stopPropagation()

        if (e.target.matches(trigger) || e.target.closest('.dropdown-content')) {
          const selector = `#${e.target.id} .dropdown-content`
          const content = document.querySelector(selector)

          if (props.alignBottom === true) {
            content.style.top = '100%'
          }

          dropdown.open(selector)

          if (props.onOpen !== undefined) {
            props.onOpen()
          }
        }
        else {
          if (props.onClose !== undefined) {
            dropdown.close(undefined, props.onClose)
          }
        }
      })
    },

    open: (selector) => {
      if (dropdown.current !== selector) {
        dropdown.close()
      }

      dropdown.current = selector

      const m = document.querySelector(selector)

      m.style.display = 'block'

      setTimeout(function () {
        m.style.transform = 'scale(1)'
        m.style.opacity = '1'

        dropdown.isOpen = true
      }, 0)
    },

    close: (selector, callback) => {
      if (selector === undefined) {
        const m = document.querySelectorAll(dropdown.current)

        m.forEach(function (el) {
          el.style.transform = ''
          el.style.opacity = ''
          
          setTimeout(function () {
            el.style.display = ''
            
            if (callback !== undefined) {
              if (dropdown.isOpen !== false) {
                callback()
              }
            }
            
            dropdown.isOpen = false
          }, 200)
        })
      }

      else {
        const m = document.querySelectorAll('.dropdown-content')

        m.forEach(function (el) {
          el.classList.remove('show')

          el.style.display = ''

          if (callback !== undefined) callback()
        })
      }
    }
  }

  window.dropdown = dropdown
})(window);