<?php
// Front-end

Route::get('/', ['as' => 'site.index.index', function () {
  return view('site.index.index');
}]);

Route::get('/contato', ['as' => 'site.pages.contato', function () {
  return view('site.pages.contato');
}]);


Route::get('/quem-somos', ['as' => 'site.pages.quem-somos', function () {
  return view('site.pages.quem-somos');
}]);

Route::get('/exames', ['as' => 'site.pages.exames', function () {
  return view('site.pages.exames');
}]);

Route::get('/servicos', ['as' => 'site.pages.servicos', function () {
  return view('site.pages.servicos');
}]);
