const mix = require('laravel-mix');

mix
  // Third-part js libs
  .scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/swiper/swiper-bundle.min.js',
    'resources/js/swiper.js',
    "resources/js/barra.js",
    "resources/js/accordion.js",
    "resources/js/aos.js",
    'public/js/site/global.js',
  ], 'public/js/site/custom.js')

  // Development js
  .js('resources/js/site.js', 'public/js/site/global.js')

  // Development sass
  .sass('resources/sass/global.scss', 'public/css/site/global.css')
  .options({
    autoprefixer: {
      options: {
        browsers: [ 'last 40 versions' ],
        grid: true
      }
    }
  })

  // Third-part css libs + development compiled css
  .styles([
    'node_modules/@fortawesome/fontawesome-free/css/all.min.css',
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/swiper/swiper-bundle.min.css',
    'public/css/site/global.css'
  ], 'public/css/site/custom.css')

  .version()

  .browserSync('http://127.0.0.1:8000/');
