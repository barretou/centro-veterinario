<!DOCTYPE html>
<html lang="pt-br">
<head>
	@component('components.site.head')
	@endcomponent
</head>

<body>
	{{-- <header>
		@component('components.site.header')
		@endcomponent
	</header> --}}

	<main id="top">
		@yield('content')
	</main>

	<footer>
		@component('components.site.footer')
		@endcomponent
	</footer>


	@component('components.site.scripts')
  @endcomponent

  <script>
    var swiper = new Swiper(".mySwiperProfessionals", {
        slidesPerView: 3,
        spaceBetween: 30,
        breakpoints: {  
        '800': {
        slidesPerView: 3,
        spaceBetween: 30, },
        '500': {
        slidesPerView: 1,
        spaceBetween: 40,},
        },
        slidesPerGroup: 3,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
</script>
</body>
</html>
