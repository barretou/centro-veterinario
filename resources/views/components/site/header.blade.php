    <!-- Navbar-->
    <nav class="navbar sticky-top autohide">
      <div class="container">
        <section class="wrapper">
          <h2 class="brand d-flex align-items-center mb-0">
            <img src="img/LogoHC01.png" alt="" style="height: 3rem" />
          </h2>
          <button type="button" class="burger" id="burger">
            <span class="burger-line"></span>
            <span class="burger-line"></span>
            <span class="burger-line"></span>
            <span class="burger-line"></span>
          </button>
          <div class="menu" id="menu">
            <ul class="menu-inner mb-0">
              <li class="menu-item">
                <a href="{{ route('site.index.index') }}" class="menu-link active">Home</a>
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.quem-somos') }}" class="menu-link">Quem Somos</a>
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.servicos') }}" class="menu-link">Nossos Serviços</a>
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.contato') }}" class="menu-link">Fale Conosco</a>
              </li>
              <li class="menu-item">
                <a href="" class="menu-link"
                  >Agendar Consulta</a
                >
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.exames') }}" class="menu-link orange-color"
                  ><img src="/img/cardiac.png" alt=""> Resultados de Exames</a
                >
              </li>
            </ul>
          </div>
        </section>
      </div>
    </nav>

<!--
your link model
href="{{ route('site.index.index') }}"
 -->
