<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="author" content="Bredi - http://www.bredi.com.br">

<title>HC Centro Veterinário</title>

{{-- SEO --}}
@component('components.site.seo')
@endcomponent

{{-- site theme --}}
<meta name="msapplication-navbutton-color" content="#FFFFFF" />
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="apple-mobile-web-app-status-bar-style" content="#FFFFFF">
<meta name="theme-color" content="#FFFFFF">

{{-- favicon --}}
<link rel="icon" sizes="192x192" href="/img/pata.png">
<link rel="icon" href="/img/logo/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="/img/pata.png" type="image/x-icon" />

{{-- fonts --}}
<link href="/fonts/fonts.css" rel="stylesheet">

{{-- custom stylesheets  --}}
<link href="{{ mix('/css/site/custom.css') }}" rel="stylesheet" media />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<!--AOS CSS-->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

<!--CDN Google Fonts (Mitr)-->
<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link
    href="https://fonts.googleapis.com/css2?family=Mitr:wght@300;400;500;600&display=swap"
    rel="stylesheet"
/>

<!--CDN Google Fonts (Poppins)-->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap" rel="stylesheet">

{{-- page styles  --}}
@yield('styles')
