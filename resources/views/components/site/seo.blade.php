<!-- Primary Meta Tags -->
<meta name="title" content="HC - Centro Veterinario">
<meta name="description" content=" Hospital veterinário com atendimento 24 horas">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://hccentroveterinario.sitebeta.com.br">
<meta property="og:title" content="HC - Centro Veterinario">
<meta property="og:description" content="Hospital veterinário com atendimento 24 horas">
<meta property="og:image" content="/img/logo.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://hccentroveterinario.sitebeta.com.br/">
<meta property="twitter:title" twitter:title content="HC - Centro Veterinario">
<meta property="twitter:description" content="Hospital veterinário com atendimento 24 horas.">
<meta property="twitter:image" content="/img/logo.png">
