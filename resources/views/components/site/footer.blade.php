<div class="container-fluid color-footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-12">
        <div class="d-flex flex-column justify-content-center text-white">
          <img src="/img/LogoHC01branca.png" alt="">
          <p class="mt-2 text-center text-lg-start">HC centro Veterinário Dr. Paulo Maia 2021. Todos os direitos reservados Grupo HC ©</p>
          <p class="text-center text-lg-start orange-color">Desenvolvido por Bredi</p>
        </div>
      </div>
      <div class="col-lg-3 col-12">
        <div class="d-flex flex-column justify-content-center text-white">
          <h5 class="text-center text-lg-start">Unidade Umarizal</h5>
          <p class="mt-2">Endereço: Av. Almirante Wandenkolk 419 | Umarizal. Belém - PA. Tel : (91) 3252-2708 | 3352-8556 | 98895-8427 Email: contato@hccentroveterinario.com.br Horário de Funcionamento: aberto 24h
          </p>
        </div>
      </div>
      <div class="col-lg-3 col-12">
        <div class="d-flex flex-column justify-content-center text-white">
          <h5 class="text-center text-lg-start">Unidade Coqueiro</h5>
          <p class="mt-2">Endereço: Avenida Três Corações, 331 | Coqueiro . Ananindeua . PA Tel : (91) 3353-1943 WhatsApp : (91) 98406-6944 | recepcaocoqueiro@hccentroveterinario.com.br Horário de Funcionamento: aberto 24h
          </p>
        </div>
      </div>
      <div class="col-lg-3 col-12">
        <div class="d-flex flex-column justify-content-center align-items-center text-white">
          <h5 class="text-center text-lg-start">Institucional</h5>
          <p class="mt-2 text-center">
            <a href="">Resultado de Exames</a>  <br> 
            <a href="">Quem Somos</a>  <br> 
            <a href="">Nosso Serviço</a>  <br>
            <a href="">Fale Conosco</a>  <br>
            <a href="">Agendar Consulta</a> 
          </p>
        </div>
      </div>
    </div>
  </div>
</div>