<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>


{{-- custom scripts  --}}
<script type="text/javascript" src="{{ mix('/js/site/custom.js') }}"></script>
<noscript>Your browser is outdated or does not support JavaScript</noscript>


{{-- <script src="/resources/js/swiper.js"></script>

<script src="/resources/js/barra.js"></script>

<script src="/resources/js/accordion.js"></script>

<script src="/resources/js/aos.js"></script> --}}






@yield('scripts')
