@extends('layouts.app')

@section('content')

<div class="container" style="height: 100vh">
  <div class="row" style="height: 100vh">
    <div class="col-lg-6 col-12 d-none d-lg-block">
      <div class="h-100 img-login"></div>
    </div>

    <div class="col-lg-6 col-12 d-flex justify-content-center">
      <div class="d-flex flex-column justify-content-center" >
        <img style="width: 20rem"  src="/img/LogoHC01.png" alt="" />
        <h1 class="mt-3" style="color: #057683">Área de Exames</h1>
        <p style="color: #4a4444" class="mt-2">
          Consulte aqui os resultados de exames do seu pet, com login e
          senha fornecidos pelo HC clínica veterinária
        </p>
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label"
            >Usuário</label
          >
          <input
            type="text"
            class="form-control"
            id="exampleFormControlInput1"
            placeholder="Digite seu Usuário"
          />
        </div>
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label"
            >Senha</label
          >
          <input
            type="password"
            class="form-control"
            id="exampleFormControlInput1"
            placeholder="Digite sua senha"
          />
        </div>
        <a href="#">
          <button class="btn btn-banner w-50">Entrar</button></a
        >
      </div>
    </div>
  </div>
</div>

@endsection
