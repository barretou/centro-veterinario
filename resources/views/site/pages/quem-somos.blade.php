@extends('layouts.app')

@section('content')

    <!-- Navbar-->
    <nav class="navbar sticky-top autohide">
        <div class="container">
          <section class="wrapper">
            <h2 class="brand d-flex align-items-center mb-0">
              <img src="img/LogoHC01.png" alt="" style="height: 3rem" />
            </h2>
            <button type="button" class="burger" id="burger">
              <span class="burger-line"></span>
              <span class="burger-line"></span>
              <span class="burger-line"></span>
              <span class="burger-line"></span>
            </button>
            <div class="menu" id="menu">
              <ul class="menu-inner mb-0">
                <li class="menu-item">
                  <a href="{{ route('site.index.index') }}" class="menu-link">Home</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.quem-somos') }}" class="menu-link active">Quem Somos</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.servicos') }}" class="menu-link">Nossos Serviços</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.contato') }}" class="menu-link">Fale Conosco</a>
                </li>
                <li class="menu-item">
                  <a href="" class="menu-link"
                    >Agendar Consulta</a
                  >
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.exames') }}" class="menu-link orange-color"
                    ><img src="/img/cardiac.png" alt=""> Resultados de Exames</a
                  >
                </li>
              </ul>
            </div>
          </section>
        </div>
      </nav>

     <!--Banner Contato-->
     <div class="container-fluid bg-azul">
      <div class="container">
          <div class="row py-5">
              <div class="col-lg-12 col-12 text-white text-center pt-0 pt-lg-5 overflow-hidden" >
                  <h1 class="text-center">Quem Somos</h1>
                  <p class="p-4" data-aos="fade-left">Iniciamos nossas atividades no ano de 2000 e em 2014, após muito trabalho e dedicação
                      aos animais passamos por uma grande reformulação, viramos um hospital com atendimento 24h.</p>
              </div>
          </div>
      </div>
  </div>

  <!--Nossa História-->
  <div class="container  mt-5">
      <div class="row">
          <div class="col-lg-6 col-12 d-flex justify-content-center">
              <div class="px-5 overflow-hidden">
                  <h1><span class="verde-color">Nossa</span> <span class="orange-color">História</span></h1>
                  <p  data-aos="fade-right">Iniciamos nossa atividade no ano de 2000 e em 2014, após muito trabalho e dedicação aos animais
                      passamos por uma grande reformulação, viramos um hospital com atendimento 24 horas. Nos
                      destacamos pela visão empreendedora, oferecendo a mais completa estrutura para um atendimento
                      clínico, cirúrgico e de diagnóstico para atender as necessidades dos animais e seus
                      proprietários de uma forma mais ampla. O amor pelos animais e pela medicina veterinária sempre
                      foram a motivação para o nosso crescimento.</p>
              </div>
          </div>
          <div class="col-lg-6 col-12 d-flex justify-content-center">
              <div class="overflow-hidden">
                  <img class="w-100" src="/img/clinica.png" alt=""  data-aos="fade-left">
              </div>
          </div>
      </div>
  </div>

  <!--Missão, visão e valores-->
  <div class="container-fluid bg-cinza mt-5">
      <div class="container">
          <div class="row py-5">
              <div class="col-lg-6 col-12 d-flex justify-content-center" data-aos="fade-right">
                  <img class="w-100" src="/img/mulher-gato.png" alt="">
              </div>
              <div class="col-lg-6 col-12"  data-aos="fade-up">
                  <div>
                      <h1 class="orange-color text-center text-lg-start mt-4 mt-lg-0">Missão</h1>
                      <p class="marrom-color mt-4">Promover com excelência e ética a saúde do seu pet, atuando com
                          pioneirismo, visando a prevenção e o bem estar com qualidade de vida</p>
                  </div>
                  <div class="mt-5">
                      <h1 class="verde-color text-center text-lg-start">Visão</h1>
                      <p class="marrom-color mt-4">Se reconhecido pela excelência no atendimento de pequenos animais
                          tendo como premissa a vida como um bem.</p>
                  </div>
                  <div class="mt-5">
                      <h1 class="orange-color text-center text-lg-start">Valores</h1>
                      <p class="marrom-color mt-4">O HC Centro Veterinário Dr. Paulo Maia tem o intuito tornar-se como
                          referência em medicina veterinária de animais de companhia, superando a expectativa de seus
                          clientes através do investimento na formação de seus profissionais, em recursos tecnológicos
                          em moderna estrutura física.</p>
                      <ul class="ms-0 ps-0 mt-5 text-center text-lg-start">
                          <li class="verde-color fw-bold mb-2">Valorização da vida</li>
                          <li class="orange-color fw-bold mb-2">Ética</li>
                          <li class="verde-color fw-bold mb-2">Pioneirismo</li>
                          <li class="orange-color fw-bold mb-2">Compromisso</li>
                          <li class="verde-color fw-bold mb-2">Qualidade</li>
                          <li class="orange-color fw-bold mb-2">Humanização</li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </div>
  
  <!--Nosso Centro Médico-->
  <div class="container mt-5">
      <div class="row">
          <div class="col-lg-12 col-12 text-center"  data-aos="fade-down">
              <h1 class="orange-color">Nosso <span class="verde-color">Centro Médico</span></h1>
              <p class="marrom-color">É referência em medicina veterinária de animais de companhia com eficiência e
                  valorização da vida, além do investimento em estrutura fisica</p>
          </div>
      </div>


      <div class="row my-5">
          <div class="col-lg-12 col-12 d-flex flex-column flex-lg-row justify-content-center">
              <div class="text-center mt-2 mt-lg-0">
                  <p class="marrom-color">Recepção</p>
                  <img src="/img/01.svg" alt="">
              </div>
              <div class="d-none d-lg-block">
                  <img class="setas-baixo" src="/img/seta-baixo.svg" alt="">
              </div>
              <div class="text-center mt-2 mt-lg-0">
                  <p class="marrom-color">Atendimento</p>
                  <img src="/img/02.svg" alt="">
              </div>
              <div class="d-none d-lg-block">
                  <img class="setas-cima" src="/img/seta-cima.svg" alt="">
              </div>
              <div class="text-center mt-2 mt-lg-0">
                  <p class="marrom-color">Procedimento</p>
                  <img src="/img/03.svg" alt="">
              </div>
              <div class="d-none d-lg-block">
                  <img class="setas-baixo" src="/img/seta-baixo.svg" alt="">
              </div>
              <div class="text-center mt-2 mt-lg-0">
                  <p class="marrom-color">Recuperação</p>
                  <img src="/img/04.svg" alt="">
              </div>
          </div>
      </div>
      <div class="row mt-5">
          <div class="col-lg-12 col-12 d-flex justify-content-center">
              <button class="btn btn-orange">Agendar Consulta</button>
          </div>
      </div>
  </div>

  <style>
      .swiper {
          width: 100%;
          height: 100%;
      }

      .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #F7F7FA;

          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
      }

      .swiper-slide img {
          display: block;
          width: 100%;
          height: 100%;
          object-fit: cover;
      }
  </style>
  
  <!--Nossos Profissionais-->
  <div class="container-fluid bg-cinza mt-5">
      <div class="container">
          <div class="row py-3">
              <div class="col-lg-12 col-12">
                  <h1 class="orange-color">Nossos <span class="verde-color">Profissionais Incríveis</span></h1>
              </div>
          </div>
          <div class="row">
              <div class="col-lg-12 col-12">
                  <!-- Swiper -->
                  <div class="swiper mySwiperProfessionals">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide py-2" style="background: #F8EDEB;">
                              <div class="d-flex flex-column">
                                  <img src="/img/med 1.png" alt="">
                                  <p class="marrom-color fw-bold mt-2">Médico(a) 01 <br> <span class="orange-color">Médico Veterinário</span></p>
                              </div>
                          </div>
                          <div class="swiper-slide py-2">
                              <div class="d-flex flex-column">
                                  <img src="/img/med 2.png" alt="">
                                  <p class="marrom-color fw-bold mt-2">Médico(a) 02 <br> <span class="orange-color">Médico Veterinário</span></p>                                 
                              </div>
                          </div>
                          <div class="swiper-slide py-2" style="background: #F8EDEB;">
                              <div class="d-flex flex-column">
                                  <img src="/img/med 3.png" alt="">
                                  <p class="marrom-color fw-bold mt-2">Médico(a) 03 <br> <span class="orange-color">Médico Veterinário</span></p>
                              </div>
                          </div>
                          <div class="swiper-slide py-2">
                              <div class="d-flex flex-column">
                                  <img src="/img/med 4.png" alt="">
                                  <p class="marrom-color fw-bold mt-2">Médico(a) 04 <br> <span class="orange-color">Médico Veterinário</span></p>
                              </div>
                          </div>
                          <div class="swiper-slide py-2" style="background: #F8EDEB;">
                              <div class="d-flex flex-column">
                                  <img src="/img/med 1.png" alt="">
                                  <p class="marrom-color fw-bold mt-2">Médico(a) 05 <br> <span class="orange-color">Médico Veterinário</span></p>     
                              </div>
                          </div>
                          <div class="swiper-slide py-2">
                              <div class="d-flex flex-column">
                                  <img src="/img/med 2.png" alt="">
                                  <p class="marrom-color fw-bold mt-2">Médico(a) 06 <br> <span class="orange-color">Médico Veterinário</span></p>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-button-next"></div>
                      <div class="swiper-button-prev"></div>
                      <div class="swiper-pagination"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>



@endsection
