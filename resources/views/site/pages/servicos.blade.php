@extends('layouts.app')

@section('content')

    <!-- Navbar-->
    <nav class="navbar sticky-top autohide">
        <div class="container">
          <section class="wrapper">
            <h2 class="brand d-flex align-items-center mb-0">
              <img src="img/LogoHC01.png" alt="" style="height: 3rem" />
            </h2>
            <button type="button" class="burger" id="burger">
              <span class="burger-line"></span>
              <span class="burger-line"></span>
              <span class="burger-line"></span>
              <span class="burger-line"></span>
            </button>
            <div class="menu" id="menu">
              <ul class="menu-inner mb-0">
                <li class="menu-item">
                  <a href="{{ route('site.index.index') }}" class="menu-link">Home</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.quem-somos') }}" class="menu-link">Quem Somos</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.servicos') }}" class="menu-link active">Nossos Serviços</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.contato') }}" class="menu-link">Fale Conosco</a>
                </li>
                <li class="menu-item">
                  <a href="" class="menu-link"
                    >Agendar Consulta</a
                  >
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.exames') }}" class="menu-link orange-color"
                    ><img src="/img/cardiac.png" alt=""> Resultados de Exames</a
                  >
                </li>
              </ul>
            </div>
          </section>
        </div>
      </nav>

    <!--Banner Nossos Serviços-->
    <div class="container-fluid bg-servicos">
      <div class="container">
          <div class="row py-5">
              <div class="col-lg-12 col-12 text-white text-center pt-0 pt-lg-4 overflow-hidden" >
                  <div data-aos="fade-down">
                      <h1 class="text-center">Nossos Serviços</h1>
                      <p class="p-4">Iniciamos nossa atividade no ano de 2000 e em 2014, após muito trabalho e dedicação aos animais passamos por uma grande reformulação, viramos um hospital com atendimento 24 horas. Nos destacamos pela visão empreendedora, oferecendo a mais completa estrutura para um atendimento clínico, cirúrgico e de diagnóstico para atender as necessidades dos animais e seus proprietários de uma forma mais ampla. O amor pelos animais e pela medicina veterinária sempre foram a motivação para o nosso crescimento.</p>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="container">
      <div class="row">
          <div class="col-lg-6 col-12 d-flex justify-content-center mt-4 mt-lg-0">
              <div class="card up-card w-75 p-4 d-flex text-center bg-verde text-white">
                  <h6>Especialidades</h6>
              </div>
          </div>
          <div class="col-lg-6 col-12 d-flex justify-content-center mt-4 mt-lg-0">
              <div class="card up-card w-75 p-4 text-center verde-color">
                  <h6>Hospitalização</h6>
              </div>
          </div>                
      </div>
  </div>

  <!--Especialidades-->

  <div class="container">
      <div class="row mt-3 mt-lg-0">
          <div class="col-lg-12 col-12">
              <h2 class="text-center verde-color">Especialidades</h2>
          </div>
      </div>
      <div class="row py-4  d-flex justify-content-center">
          <div class="col-lg-6 col-12 d-flex justify-content-center paw-list overflow-hidden" >
              <ul  data-aos="fade-right">
                  <li><img src="/img/paw-list.png" alt=""> Acupuntura</li>
                  <li><img src="/img/paw-list.png" alt=""> Anestesiologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Cardiologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Dermatologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Gastroenterologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Medicina Diagnóstica</li>
                  <li><img src="/img/paw-list.png" alt=""> Medicina Felina</li>
                  <li><img src="/img/paw-list.png" alt=""> Nefrologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Odontologia</li>                   
              </ul>
          </div>
          <div class="col-lg-6 col-12 d-flex justify-content-center paw-list overflow-hidden">
              <ul  data-aos="fade-left">
                  <li><img src="/img/paw-list.png" alt=""> Oftalmologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Oncologia</li>
                  <li><img src="/img/paw-list.png" alt=""> Ortopedia</li>
                  <li><img src="/img/paw-list.png" alt=""> Hospitalização</li>
                  <li><img src="/img/paw-list.png" alt=""> Radiografia</li>
                  <li><img src="/img/paw-list.png" alt=""> Laboratório Clínico</li>
                  <li><img src="/img/paw-list.png" alt=""> Cirurgia Geral</li>
                  <li><img src="/img/paw-list.png" alt=""> Cirurgia  Ortopédica</li>
                  <li><img src="/img/paw-list.png" alt=""> Eletrocardiograma</li>
                  <li><img src="/img/paw-list.png" alt=""> Ultrassom</li>                  
              </ul>
          </div>
      </div>
  </div>

@endsection
