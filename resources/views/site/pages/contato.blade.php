@extends('layouts.app')

@section('content')

    <!-- Navbar-->
    <nav class="navbar sticky-top autohide">
        <div class="container">
          <section class="wrapper">
            <h2 class="brand d-flex align-items-center mb-0">
              <img src="img/LogoHC01.png" alt="" style="height: 3rem" />
            </h2>
            <button type="button" class="burger" id="burger">
              <span class="burger-line"></span>
              <span class="burger-line"></span>
              <span class="burger-line"></span>
              <span class="burger-line"></span>
            </button>
            <div class="menu" id="menu">
              <ul class="menu-inner mb-0">
                <li class="menu-item">
                  <a href="{{ route('site.index.index') }}" class="menu-link">Home</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.quem-somos') }}" class="menu-link">Quem Somos</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.servicos') }}" class="menu-link">Nossos Serviços</a>
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.contato') }}" class="menu-link active">Fale Conosco</a>
                </li>
                <li class="menu-item">
                  <a href="" class="menu-link"
                    >Agendar Consulta</a
                  >
                </li>
                <li class="menu-item">
                  <a href="{{ route('site.pages.exames') }}" class="menu-link orange-color"
                    ><img src="/img/cardiac.png" alt=""> Resultados de Exames</a
                  >
                </li>
              </ul>
            </div>
          </section>
        </div>
      </nav>

    <!--Banner Contato-->
    <div class="container-fluid bg-azul">
      <div class="container" data-aos="fade-down">
          <div class="row py-5">
              <div class="col-lg-12 col-12 text-white pt-0 pt-lg-5">
                  <h1 class="text-center">Fale Conosco</h1>                    
              </div>
          </div>
      </div>
  </div>


  <!--Formulários de contato-->
  <div class="container mt-5 pb-5">
      <div class="row">
          <div class="col-lg-6 col-12 h-100 d-flex flex-column justify-content-center overflow-hidden">
              <div class="my-3"  data-aos="fade-down">
                  <h2 class="orange-color">Formulário de Contato</h2>
                  <p>Você pode entrar em contato por Email, basta informar email e sua mensagem</p>
              </div>
              <form action=""  data-aos="fade-right">
                  <label class="fw-bold" for="">Email</label>
                  <input class="form-control px-3" type="text" placeholder="Digite seu Usuário">
                  <label class="fw-bold pt-3" for="">Mensagem</label>
                  <div class="input-group">
                      <textarea class="form-control" placeholder="Digite sua mensagem" aria-label="With textarea"></textarea>
                  </div>
              </form>
              <button class="btn btn-banner mt-3 w-50">Enviar</button>
          </div>
          <div class="col-lg-6 col-12 d-flex flex-column justify-content-center align-items-center mt-4 mt-lg-0 overflow-hidden">
              <div class="my-3"  data-aos="fade-left">
                  <h2 class="orange-color">Outros Canais de Contato</h2>
              </div>

              <div class="card w-75 d-flex flex-row" style="background: #F5F6F6;"  data-aos="fade-right">
                  <div>
                      <img src="/img/paw-list.png" alt="">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title" style="color: #4A4444;">Atendimentos de Urgência</h5>
                    <p class="card-text">Coqueiro (91) 8985 8783.</p>
                    <p class="card-text">Umarizal (91) 8985 8783</p>
                  </div>
                </div>

                <div class="card mt-3 w-75 d-flex flex-row" style="background: #F5F6F6;"  data-aos="fade-left">
                  <div>
                      <img src="/img/paw-list.png" alt="">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title" style="color: #4A4444;">Atendimentos de Urgência</h5>
                    <p class="card-text">Coqueiro (91) 8985 8783.</p>
                    <p class="card-text">Umarizal (91) 8985 8783</p>
                  </div>
                </div>
          </div>
      </div>
  </div>

@endsection
