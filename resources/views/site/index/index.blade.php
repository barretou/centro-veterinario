@extends('layouts.app')

@section('content')

    <!-- Navbar-->
    <nav class="navbar sticky-top autohide">
      <div class="container">
        <section class="wrapper">
          <h2 class="brand d-flex align-items-center mb-0">
            <img src="img/LogoHC01.png" alt="" style="height: 3rem" />
          </h2>
          <button type="button" class="burger" id="burger">
            <span class="burger-line"></span>
            <span class="burger-line"></span>
            <span class="burger-line"></span>
            <span class="burger-line"></span>
          </button>
          <div class="menu" id="menu">
            <ul class="menu-inner mb-0">
              <li class="menu-item">
                <a href="{{ route('site.index.index') }}" class="menu-link active">Home</a>
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.quem-somos') }}" class="menu-link">Quem Somos</a>
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.servicos') }}" class="menu-link">Nossos Serviços</a>
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.contato') }}" class="menu-link">Fale Conosco</a>
              </li>
              <li class="menu-item">
                <a href="" class="menu-link"
                  >Agendar Consulta</a
                >
              </li>
              <li class="menu-item">
                <a href="{{ route('site.pages.exames') }}" class="menu-link orange-color"
                  ><img src="/img/cardiac.png" alt=""> Resultados de Exames</a
                >
              </li>
            </ul>
          </div>
        </section>
      </div>
    </nav>

    <!-- Banner-->
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-12">
          <div class="banner-img">
            <div class="container h-100">
              <div class="d-flex flex-column justify-content-center h-100">
                <h1 class="text-white" data-aos="fade-down">
                  No HC oferecemos Tudo o <br />
                  Que o seu Pet Precisa.
                </h1>
                <button class="btn btn-banner mt-2 w-25" data-aos="fade-up">Ver Serviços</button>
              </div>
            </div>
          </div>
        </div>
      </div>      
  </div>

  <!-- Cards-->
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-12">
        <div class="card d-flex flex-row align-items-center up-card mt-4 mt-lg-0">
          <div class="mx-2">
            <img src="/img/24 HORAS.svg" alt="" />
          </div>
          <div class="card-body">
            <h5 class="card-color-text">Hospitalização</h5>
            <p class="card-color-text">
              Atendimento 24h, com infraestrutura completa para atender o seu
              pet.
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-12 mt-4 mt-lg-0">
        <div
          class="card d-flex flex-row align-items-center up-card mt-4 mt-lg-0"
        >
          <div class="mx-2">
            <img src="/img/RESULTADOS ONLINE.svg" alt="" />
          </div>
          <div class="card-body">
            <h5 class="card-color-text">Resultados Online</h5>
            <p class="card-color-text">
              Você têm a opção de ver os resultados de exames online no nosso
              sistema.
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-12 mt-4 mt-lg-0">
        <div
          class="card d-flex flex-row align-items-center up-card mt-4 mt-lg-0"
        >
          <div class="mx-2">
            <img src="/img/ESPECIALIDADES.svg" alt="" />
          </div>
          <div class="card-body">
            <h5 class="card-color-text">Especilidades</h5>
            <p class="card-color-text">
              identificar e tratar doenças na cavidade oral dos animais, e
              manter a saúde do animal.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--Oferecimento HC-->
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex align-items-center">
        <img class="w-100" src="/img/gato-bolinha.png"  data-aos="fade-right" alt="" />
      </div>
      <div
        class="
          col-lg-6
          d-flex
          flex-column
          justify-content-center
          mb-5 mb-lg-0
        "
      >
        <h1 class="text-center text-lg-start" style="color: #6d6d6d">
          No HC oferecemos <br />
          Tudo o Que o seu <br />
          Pet Precisa.
        </h1>
        <p style="color: #4a4444">
          O diagnóstico precoce e tratamento intensivo imediato fazem parte da
          nossa rotina.O diagnóstico precoce e tratamento intensivo imediato
          fazem parte da nossa rotina. O diagnóstico precoce e tratamento
          intensivo imediato fazem parte da nossa rotina. O diagnóstico
          precoce e tratamento intensivo imediato fazem parte da nossa rotina.
          O diagnóstico precoce e tratamento intensivo imediato fazem parte da
          nossa rotina.
        </p>
        <div class="d-flex justify-content-center justify-content-lg-start">
          <button class="btn btn-orange w-50">Ver serviços</button>
        </div>
      </div>
    </div>
  </div>

  <!--Atendimento 24h-->
  <div class="container-fluid bg-azul">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-6 col-12 d-flex justify-content-center">
        <div
          class="
            d-flex
            flex-column
            align-items-center
            justify-content-end
            bg-cat
          "
        >
          <h1 class="text-white">Atendimento 24h</h1>
          <button class="btn btn-orange w-50 mb-3">Contato</button>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid" style="background-color: #f5f6f6">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-12 d-flex justify-content-center mt-3">
          <div class="d-flex flex-column mt-5 text-center text-lg-start" style="color: #4a4444">
            <h5 class="mt-5">Atendimentos de Urgência</h5>
            <p>Coqueiro (91) 8985 8783</p>
            <p>Umarizal (91) 8985 8783</p>
          </div>
        </div>
        <div class="col-lg-6 col-12 d-flex justify-content-center mt-3">
          <div class="d-flex flex-column mt-5 text-center text-lg-start" style="color: #4a4444">
            <h5 class="mt-5">Avaliações clínicas e Exames</h5>
            <p>Coqueiro (91) 8985 8783</p>
            <p>Umarizal (91) 8985 8783</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--Dúvidas frequentes-->
  <div class="container my-5">
    <div class="row">
      <div class="d-block d-lg-none col-lg-6 col-12 overflow-hidden" >
        <div class="h-100 d-flex justify-content-center align-items-start" data-aos="fade-up">
          <img class="position-lg-absolute w-100" src="/img/pug.png" alt="" />
        </div>
      </div>
      <div class="col-lg-6 col-12">
        <h1 class="my-5 text-center text-lg-start" style="color: #6d6d6d">
          Duvidas <br />
          Frequentes
        </h1>
        <div>
          <div class="wrapper text-center text-lg-start">
              <div class="block one">
                  <div class="block__item">
                      <div class="block__title">Dúvida frequente 01</div>
                      <div class="block__text px-3 text-start px-lg-0">
                          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Numquam rem quia illo maxime voluptates. Tenetur voluptatem possimus perferendis molestias optio dolores quidem voluptatibus, harum expedita, vero eius iste vitae eveniet.Nisi nobis omnis pariatur consectetur magni animi doloremque assumenda! Reprehenderit temporibus tenetur fugit natus velit culpa cumque ipsam impedit distinctio eveniet enim, ab ipsum maiores. Id fugit at quos mollitia.
                      </div>
                  </div>
                  <div class="block__item">
                      <div class="block__title">Dúvida frequente 02</div>
                      <div class="block__text px-3 text-start px-lg-0">
                          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Numquam rem quia illo maxime voluptates. Tenetur voluptatem possimus perferendis molestias optio dolores quidem voluptatibus, harum expedita, vero eius iste vitae eveniet.Nisi nobis omnis pariatur consectetur magni animi doloremque assumenda! Reprehenderit temporibus tenetur fugit natus velit culpa cumque ipsam impedit distinctio eveniet enim, ab ipsum maiores. Id fugit at quos mollitia.
                      </div>
                  </div>
                  <div class="block__item">
                      <div class="block__title">Dúvida frequente 03</div>
                      <div class="block__text px-3 text-start px-lg-0">
                          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Numquam rem quia illo maxime voluptates. Tenetur voluptatem possimus perferendis molestias optio dolores quidem voluptatibus, harum expedita, vero eius iste vitae eveniet.Nisi nobis omnis pariatur consectetur magni animi doloremque assumenda! Reprehenderit temporibus tenetur fugit natus velit culpa cumque ipsam impedit distinctio eveniet enim, ab ipsum maiores. Id fugit at quos mollitia.
                      </div>
                  </div>
              </div>
          </div>        
        </div>
      </div>
      <div class="col-lg-6 col-12 d-none d-lg-block"  data-aos="fade-up">
        <div class="h-100 d-flex justify-content-center align-items-start">
          <img class="position-lg-absolute" height="300rem" src="/img/pug.png" alt="" />
        </div>
      </div>
    </div>
  </div>

  <!--Depoimentos-->
  <div class="container-fluid bg-cardiac mt-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-12">
          <div class="title text-center pt-5">
            <h1 class="text-white">Depoimentos</h1>
          </div>
        </div>
      </div>
      <div class="row overflow-hidden">
        <div class="col-lg-12 col-12 mb-5" data-aos="fade-right">
          <div>
            <!-- Swiper -->
            <div class="swiper mySwiper mt-5">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div class=" d-flex flex-column p-4">
                    <h4 style="color: #6d6d6d" class="text-center">
                      Luan Maos
                    </h4>
                    <h5 style="color: #057683" class="text-center">Tutor</h5>
                    <p class="px-5" style="color: #6d6d6d">
                      O diagnóstico precoce e tratamento intensivo imediato
                      fazem parte da nossa rotina.O diagnóstico precoce e
                      tratamento intensivo imediato fazem parte da nossa rotina.
                      O diagnóstico precoce e tratamento intensivo imediato
                      fazem parte da nossa rotina.
                    </p>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class=" d-flex flex-column p-4">
                    <h4 style="color: #6d6d6d" class="text-center">
                      Catarina Souza
                    </h4>
                    <h5 style="color: #057683" class="text-center">Tutora</h5>
                    <p class="px-5" style="color: #6d6d6d">
                      O diagnóstico precoce e tratamento intensivo imediato
                      fazem parte da nossa rotina.O diagnóstico precoce e
                      tratamento intensivo imediato fazem parte da nossa rotina.
                      O diagnóstico precoce e tratamento intensivo imediato
                      fazem parte da nossa rotina.
                    </p>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="d-flex flex-column p-4">
                    <h4 style="color: #6d6d6d" class="text-center">
                      Ana Paula
                    </h4>
                    <h5 style="color: #057683" class="text-center">Tutora</h5>
                    <p class="px-5" style="color: #6d6d6d">
                      O diagnóstico precoce e tratamento intensivo imediato
                      fazem parte da nossa rotina.O diagnóstico precoce e
                      tratamento intensivo imediato fazem parte da nossa rotina.
                      O diagnóstico precoce e tratamento intensivo imediato
                      fazem parte da nossa rotina.
                    </p>
                  </div>
                </div>
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
              <div class="swiper-pagination"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--Galertia dos Pacientes-->
  <div class="container">
    <div class="row  my-5">
      <div class="col-lg-12 text-center">
        <h1 style="color:#EF7A68">Galeria dos Pacientes</h1>
        <p style="color: #6D6D6D;">Postagens autorizadas retiradas do nosso Insta, <br> @hccentroveterinario</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-12 overflow-hidden">
        <section class="gallery-grid" data-aos="fade-left">
          <div style="background-image: url('/img/so-o-luxo-01.png');"></div>
          <div style="background-image: url('/img/so-o-luxo-02.png');"></div>
          <div style="background-image: url('/img/so-o-luxo-03.png');"></div>
          <div style="background-image: url('/img/so-o-luxo-04.png');"></div>
        </section>
      </div>
    </div>
  </div>

  <!--Footer-->
  <div class="container mt-5">
    <div class="row  bg-footer h-100 py-5">
      <div class="col-lg-4 col-12 h-100">
        <div class="d-flex justify-content-center align-items-center h-100">
          <h1 class="text-white text-center">Nossas <br> Redes</h1>
        </div>
      </div>
      <div class="col-lg-8 col-12">
        <div class="text-white d-flex flex-column justify-content-center h-100">
          <p class="ms-1 text-center text-lg-start"><img height="20rem" src="/img/Icon feather-instagram.svg" alt=""> @hccentroveterinario</p>
          <p class="text-center text-lg-start"><img height="25rem" src="/img//Icon awesome-facebook.svg" alt=""> @hccentroveterinario.com.br</p>
        </div>
      </div>
    </div>
  </div>

@endsection
